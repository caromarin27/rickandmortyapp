import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'
import { EpisodesComponent } from '@episodes/episodes.component';
import { EpisodeDetailsComponent } from '@episodes/episode-details/episode-details.component';
import { EpisodeListComponent } from '@episodes/episode-list/episode-list.component';


const components = [
  EpisodesComponent, 
  EpisodeDetailsComponent,
  EpisodeListComponent  
];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [...components]
})
export class EpisodesModule { }
