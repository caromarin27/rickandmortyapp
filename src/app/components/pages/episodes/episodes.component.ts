import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { iEpisode } from '@app/shared/interfaces/episode.interface';


@Component({
  selector: 'app-episodes',
  template: `
  <div class="card bg-dark">
    <div class="card-inner">
        <div class="header">
            <a [routerLink]="['/episode-details', episode.id]">
            <h4>{{episode.name | slice: 0:15}}</h4>
            </a>
            <h4 class="text-muted white">{{episode.episode}}</h4>
            <small class="text-muted white">{{episode.air_date | date}}</small>
        </div>
    </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class EpisodesComponent {
  @Input() episode: iEpisode;

}
