import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { iEpisode } from '@app/shared/interfaces/episode.interface';
import { ActivatedRoute } from '@angular/router';
import { EpisodesService } from '@app/shared/services/episodes.service';
import { take } from 'rxjs/operators';
import { Location } from '@angular/common';

@Component({
  selector: 'app-episode-details',
  templateUrl: './episode-details.component.html',
  styleUrls: ['./episode-details.component.scss']
})
export class EpisodeDetailsComponent implements OnInit {

  episode$: Observable<iEpisode>;

  constructor(private route: ActivatedRoute,
    private episodeSrv: EpisodesService,
    private location: Location) { }

  ngOnInit(): void {
    this.route.params.pipe(take(1)).subscribe((params)=>{
      const id = params['id'];
      this.episode$ = this.episodeSrv.getEpisodeDetails(id);
    })
  }


  goBack():void{
    this.location.back();
  }

}
