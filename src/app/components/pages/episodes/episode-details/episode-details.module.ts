import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EpisodeDetailsRoutingModule } from './episode-details-rounting.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EpisodeDetailsRoutingModule
  ]
})
export class EpisodeDetailsModule { }
