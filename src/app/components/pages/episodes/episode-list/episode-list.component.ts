import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { iEpisode } from '@app/shared/interfaces/episode.interface';
import { EpisodesService } from '@app/shared/services/episodes.service';
import { ActivatedRoute, Router, NavigationEnd, ParamMap } from '@angular/router';
import { take, filter } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';

type RequestInfo = {
  next: string;
};

@Component({
  selector: 'app-episode-list',
  templateUrl: './episode-list.component.html',
  styleUrls: ['./episode-list.component.scss']
})
export class EpisodeListComponent implements OnInit {

  episodes: iEpisode[] = [];

info: RequestInfo = {
  next: null,
};

showGoUpButton: boolean = false;

private pageNum = 1;
private query: string;
private hideScrollHeight = 150;
private showScrollHeight = 300;


  constructor(
    @Inject(DOCUMENT) private document: Document,
    private episodeSrv: EpisodesService,
    private route: ActivatedRoute,
    private router: Router) {
      this.onUrlChanged();
    }

  ngOnInit(): void {
    this.filterEpisodesByQuery();
  }

  @HostListener('window:scroll', [])
  onWindowScroll():void {
    const yOffSet = window.pageYOffset;
    if((yOffSet || this.document.documentElement.scrollTop || this.document.body.scrollTop) > this.showScrollHeight){
      this.showGoUpButton = true;
    }else if(this.showGoUpButton && (yOffSet || this.document.documentElement.scrollTop || this.document.body.scrollTop) < this.hideScrollHeight){
      this.showGoUpButton = false;
    }
  }

  onScrollDown(): void {
    if(this.info.next){
      this.pageNum++;
      this.getEpisodesFromService();
    }
  }

  onScrollUp():void {
    this.document.body.scrollTop = 0; // Safari Browser
    this.document.documentElement.scrollTop = 0; //Other Browsers
  }

  private filterEpisodesByQuery(): void{
    this.route.queryParams.pipe(take(1)).subscribe((params: ParamMap) => {
      this.query = params['q'];
      this.getEpisodesFromService();
    });
  }

  private getEpisodesFromService(): void{
    this.episodeSrv.searchEpisodes(this.query, this.pageNum)
    .pipe(take(1))
    .subscribe((res: any) => {
      if(res?.results?.length) {
        const { info, results } = res;
        this.episodes = [...this.episodes, ...results];
        this.info = info;
      }else {
        this.episodes = [];
      }
    })
  }

  private onUrlChanged(): void{
    this.router.events.pipe(
      filter((event) =>  event instanceof NavigationEnd))
      .subscribe(() => 
        {
          this.episodes = [];
          this.pageNum = 1;
          this.filterEpisodesByQuery();
        })
  }

  
}
