import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-form-search',
  template: `
  <input 
  #inputSearch
  autofocus
  type="text"
  class="form-control-lg"
  placeholder="Search..."
  (keyup)="onSearchCharacters(inputSearch.value)"
  />
  `,
  styles: ['input {width: 100%;}']
})
export class FormSearchComponent implements OnInit {
episodeRoute: boolean = true;


  constructor(private route: ActivatedRoute,
    private router: Router,
    private location: Location) {}

  ngOnInit(): void {
    this.hideSearch();
  }

  hideSearch() {
    let routeUrl = this.route.snapshot.url;
    console.log("routeUrl", routeUrl)
      // if(routeUrl == '/episode-list'){
      //   console.log("routeUrl", routeUrl)
      // }else{
      //   console.log("ruta invalida")
      // }
    }

  onSearchCharacters(value: string){
    if(value && value.length > 3){
      this.router.navigate(['/character-list'], {
        queryParams: {q: value}
      })
  }
}


}
