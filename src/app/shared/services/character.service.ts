import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { iCharacter } from '@shared/interfaces/character.interface';
import { environment } from '@environment/environment';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }

  searchCharacters(query = '', page = 1){
    const filter = `${environment.baseUrlAPI + 'character'}/?name=${query}&page=${page}`;
    return this.http.get<iCharacter[]>(filter);
  }

  getDetails(id:number){
    return this.http.get<iCharacter>(`${environment.baseUrlAPI + 'character'}/${id}`);
  }
}
