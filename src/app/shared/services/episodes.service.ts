import { Injectable } from '@angular/core';
import { environment } from '@environment/environment';
import { HttpClient } from '@angular/common/http';
import { iEpisode } from '../interfaces/episode.interface';

@Injectable({
  providedIn: 'root'
})
export class EpisodesService {

constructor(private http: HttpClient) { }

searchEpisodes(query = '', page = 1){
  const filterEpisodes = `${environment.baseUrlAPI + 'episode'}/?name=${query}&page=${page}`;
  return this.http.get<iEpisode[]>(filterEpisodes);
}

getEpisodeDetails(id:number){
  return this.http.get<iEpisode>(`${environment.baseUrlAPI + 'episode'}/${id}`);
}

}
